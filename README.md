[Leap Motion Vasefication](https://github.com/tholapz/leappottery)
is a simple modeling tool written in JavaScript. It has Leap Motion device support as well as mouse interface. It uses Three.js library to render the result. (http://learningthreejs.com/blog/2011/12/20/boilerplate-for-three-js/).

# Get Started
```
git clone https://github.com/tholapz/leappottery.git
```